import yargs from "yargs";
import { faker } from "@faker-js/faker/locale/uk";

import bootstrap from "../utils";

interface NOTE {
    NAME: string;
    TEL: string;
    BDAY: [number, number, number];
}

const notebookStack: NOTE[] = [];

async function performInput(text: string[]): Promise<any> {
    const performedText = text[0] === "notebook" ? text.slice(1) : text;
    return yargs(!performedText.length ? ["--help"] : performedText)
        .scriptName("notebook")
        .version("default")
        .usage("$0 <cmd> [args]")
        .command("clear", "Clear console", {}, () => {
            console.clear();
        })
        .command(
            "add",
            "Add new note",
            {
                name: {
                    alias: "n",
                    description:
                        "Name of the person. Could be unique in notebook",
                    required: true,
                    type: "string",
                },
                "phone-number": {
                    alias: "t",
                    description: "Phone number of the person",
                    required: true,
                    type: "string",
                },
                "birth-date": {
                    alias: "b",
                    description: "Birth date of the person (separated by -)",
                    required: true,
                    type: "string",
                },
            },
            (args) => {
                const [day, month, year] = args["birth-date"].split("-");
                notebookStack.push({
                    NAME: args.name,
                    TEL: args["phone-number"],
                    BDAY: [Number(day), Number(month), Number(year)],
                });
                console.log("Note added");
                console.log("Notebook:", notebookStack);
            }
        )
        .command("remove", "Remove last note", {}, () => {
            const removed = notebookStack.pop();
            console.log("Note removed:", removed);
            console.log("Notebook:", notebookStack);
        })
        .command("reveal", "Reveal all notes", {}, () => {
            console.log("Notebook:", notebookStack);
        })
        .command(
            "fill",
            "Quickly fill notebook with random data (faker)",
            {
                amount: {
                    alias: "c",
                    description: "How many notes to add",
                    default: 10,
                    type: "number",
                },
            },
            (args) => {
                for (let i = 0; i < args.amount; i++) {
                    notebookStack.push({
                        NAME: `${faker.person.firstName()} ${faker.person.lastName()}`,
                        TEL: faker.phone.number(),
                        BDAY: [
                            faker.date
                                .between({
                                    from: "1950-01-01",
                                    to: "2000-01-01",
                                })
                                .getDay(),
                            faker.date
                                .between({
                                    from: "1950-01-01",
                                    to: "2000-01-01",
                                })
                                .getMonth(),
                            faker.date
                                .between({
                                    from: "1950-01-01",
                                    to: "2000-01-01",
                                })
                                .getFullYear(),
                        ],
                    });
                }
                console.log("Notebook filled with random data");
                console.log("Notebook:", notebookStack);
            }
        )
        .command(
            "show",
            "show all notes, which month of birth is equal to typed",
            {
                month: {
                    alias: "m",
                    description: "Month of birth to show",
                    required: true,
                    type: "number",
                },
            },
            (args) => {
                const filtered = notebookStack.filter(
                    (note) => note.BDAY[1] === args.month
                );
                if (!filtered.length) console.log("No notes found");
                else console.log("Filtered notebook:", filtered);
            }
        )
        .help()
        .exitProcess(false)
        .fail((msg, err, y) => {
            console.error(msg);
            if (err) throw err;
            y.showHelp();
        })
        .showHelpOnFail(true)
        .parse();
}

bootstrap(performInput, "notebook");
